package com.example.geoquiz;

public class Question {
    private int mTextRes; // holds resource ID and not the question itself
    private boolean mAnswerTrue;


    public Question(int mTextRes, boolean mAnswerTrue) {
        this.mTextRes = mTextRes;
        this.mAnswerTrue = mAnswerTrue;
    }

    public int getmTextRes() {
        return mTextRes;
    }

    public void setmTextRes(int mTextRes) {
        this.mTextRes = mTextRes;
    }

    public boolean ismAnswerTrue() {
        return mAnswerTrue;
    }

    public void setmAnswerTrue(boolean mAnswerTrue) {
        this.mAnswerTrue = mAnswerTrue;
    }
}
