package com.example.geoquiz;

public class User {
    private int score;
    private String name;

    public User(String username) {
        name = username;
        score = 0;
    }

    public void incrementScore() {
        score++;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
